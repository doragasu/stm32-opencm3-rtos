PROJECT = cm3test

DEVICE=stm32f401cc
OOCD_FILE = target/stm32f4x.cfg
BUILD_DIR = build
RTOS_DIR = $(HOME)/src/github/FreeRTOS-Kernel
OPENCM3_DIR = $(HOME)/src/github/libopencm3
SHARED_DIR =
CFLAGS+=-DLOG_LEVEL_OUTPUT=LOG_LV_VERBOSE

# Application sources
CFILES = main.c rtos_hooks.c log.c

# FreeRTOS kernel sources
CFILES += croutine.c event_groups.c list.c queue.c stream_buffer.c tasks.c timers.c

# FreeRTOS port related files
CFILES += $(RTOS_DIR)/portable/MemMang/heap_4.c
CFILES += $(RTOS_DIR)/portable/GCC/ARM_CM4F/port.c
INCLUDES += -I$(RTOS_DIR)/include -I$(RTOS_DIR)/portable/GCC/ARM_CM4F

VPATH += $(RTOS_DIR) $(SHARED_DIR)
INCLUDES += $(patsubst %,-I%/include, . $(SHARED_DIR))

include $(OPENCM3_DIR)/mk/genlink-config.mk
include ocm3_rules.mk
include $(OPENCM3_DIR)/mk/genlink-rules.mk

# Simple rule for flashing using dfu bootloader and dfu-util
.PHONY: all dfu size
all: size

dfu: $(PROJECT).bin
	dfu-util -a0 -s 0x08000000:leave -D $<

size: $(PROJECT).elf
	@echo -e "\n\033[;32m### MEMORY STATS ###\033[m\033[1m" && size $< && echo -ne "\033[m"
