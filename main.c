#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <FreeRTOS.h>
#include <task.h>

#include "log.h"

static void clock_setup(void)
{
	rcc_clock_setup_pll(&rcc_hse_25mhz_3v3[RCC_CLOCK_3V3_84MHZ]);
}

static void led_setup(void)
{
	rcc_periph_clock_enable(RCC_GPIOC);
	gpio_mode_setup(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO13);
}

static void blinker_tsk(void *arg)
{
	uint32_t sleep_ms = (uint32_t)arg;

	LOGD("heap: %ld, free: %ld", (long)configTOTAL_HEAP_SIZE,
			(long)xPortGetFreeHeapSize());
	while (true) {
		vTaskDelay(pdMS_TO_TICKS(sleep_ms));
		gpio_toggle(GPIOC, GPIO13);
	}
}

static void logger_tsk(void *arg)
{
	uint32_t id = (uint32_t)arg;
	uint32_t iteration = 0;

	while (true) {
		LOGI("id: %d, count: %d", id, iteration++);
		vTaskDelay(pdMS_TO_TICKS(100 * id));
	}
}

int main(void)
{
	clock_setup();
	led_setup();
	log_init(115200);

	// Spawn some tasks to write to log at different intervals
	xTaskCreate(logger_tsk, "logger1", 512, (void*)1, 3, NULL);
	xTaskCreate(logger_tsk, "logger2", 512, (void*)2, 3, NULL);
	xTaskCreate(logger_tsk, "logger3", 512, (void*)3, 3, NULL);
	// And of course we need a blinking LED!
	xTaskCreate(blinker_tsk, "blinker", 512, (void*)1000, 4, NULL);

	vTaskStartScheduler();

	// Should not reach here unless a task calls vTaskEndScheduler()
	while (true);

	return 0;
}
