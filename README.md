# Introduction

This is a simple template for STM32 projects using [libopencm3](https://github.com/libopencm3/libopencm3) and [FreeRTOS](https://github.com/FreeRTOS/FreeRTOS). It also includes a blazing fast DMA-based logger module.

# Setup

## Toolchain

You will need an ARM toolchain suitable for your STM32 device. Note your toolchain must generate binaries for bare-metal targets (i.e. not for Linux targets). Usually you will have a working one in your favorite Linux distro repositories. For ArchLinux you can install `arm-none-eabi-gcc` and `arm-none-eabi-newlib`.

You will also need some kind of tool to download the generated binaries to your board. The bare minimum is `dfu-util` to download the firmware using the internal STM32 bootloader. But some more elaborate means for debugging (such as ST-Link2 or CMSIS-DAP) are recommended.

**NOTE**: this repository has been tested with GCC version 11.1.0. If you have build problems, try changing to this GCC version.

## libopencm3

Clone the library in your favorite directory and build it:

```
$ git clone https://github.com/libopencm3/libopencm3.git
$ cd libopencm3
$ make
```

**NOTE**: this repository has been tested with library commit `470a1394a8`, so you can check out this commit if having problems with other one.

## FreeRTOS

Clone the FreeRTOS Kernel files to your favorite directory. You do not need to build them now, the project Makefile will take care of it.

```
$ git clone https://github.com/FreeRTOS/FreeRTOS-Kernel.git
```

**NOTE**: this repository has been tested with FreeRTOS tag `V10.4.4`. So make sure to checkout this tag if having problems.

# Usage

## Building and downloading the template project

Once you have setup the environment as briefly explained above, clone this repository. Then edit the Makefile and change `RTOS_DIR` and `OPENCM3_DIR` to point where you have installed their repos. Also make sure to change `DEVICE` to the MCU you are using and `OOCD_FILE` to the openocd configuration file for your board (you can ignore this if not using openocd). If you want to change the project name, edit also the `PROJECT` variable.

The `FreeRTOSConfig.h` file has been tailored for baseline Cortex-M4 devices (such as STM32F401). You might need to edit it to change the CPU clock (default 84 MHz), the tick rate (100 Hz), the heap size (32 KiB) and in some cases the interrupt priorities to match your device.

When done with the edits, build the firmware with `make` command.

To upload the built firmware to the device, the simplest way is putting the MCU in DFU download mode (using BOOT0 switch while booting the board) and then typing `make dfu`. This requires that `dfu-util` is installed.

Upon flashing the firmware, if the board has a LED attached to pin 13 of port C, it should start blinking (1 second on, 1 second off). If you connect an UART Rx pin to PA2 and open your favorite terminal viewer (screen, minicom, putty...) configured for 115200 bauds, you should also see a bunch of lines beautifully output to the log trace.

## Customizing the project

You can use this project as the base and customize it to add whatever you need. Typically you will have to add your source files to the `CFILES` variable, and whatever directories with sources you add to the `SHARED_DIR` variable.

# Limitations

## Compatibility

The project is tailored for STM32F4 devices. Until I figure out how to improve portability, using a different family will require some modifications.

## Logger module

The logger uses DMA with a 1024 bytes long (by default, can be changed) buffer. If more data must be output while a DMA transfer is active, the data is appended to the buffer and when the transfer in progress finishes, the remaining data is queued to the DMA and sent to the UART. Under current implementation, this buffer is not cleared until a transfer ends and there is no more pending data. This can cause that very log intensive applications exhaust the buffer. If this happens, then logs are discarded (and lost) until the in-progress transfer completes. So if your application is log intensive and you cannot afford to loose a single log byte, this log module is not for you. I might add a build option to disable DMA and use the classic and slow polling approach to send the text (blocking when UART is busy) in a future update. I also would like to improve the current approach by using a circular buffer.
