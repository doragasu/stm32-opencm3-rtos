/************************************************************************//**
 * Log module.
 *
 * Usage: call log_init() once. Then use the LOGE(), LOGW(), LOGI(), LOGD()
 * and LOGV() macros to output message in the error, warning, info, debug
 * and verbose log levels.
 *
 * The LOG_LEVEL_OUTPUT definition sets the maximum level of messages that
 * will be displayed. Default value is LOG_LV_INFO, that filters out debug
 * and verbose levels, and shows the others. You can change this macro
 * before including this module (for example in the Makefile defines).
 ****************************************************************************/

#ifndef __LOG_H__
#define __LOG_H__

#include <stdint.h>
#include <stdio.h>

// Only logs with level less or equal than this will be printed
#ifndef LOG_LEVEL_OUTPUT
#define LOG_LEVEL_OUTPUT LOG_LV_INFO
#endif

enum log_level {
    LOG_LV_NONE,
    LOG_LV_ERROR,
    LOG_LV_WARN,
    LOG_LV_INFO,
    LOG_LV_DEBUG,
    LOG_LV_VERBOSE,
    __LOG_LV_MAX
};

// Terminal color definitions and macros
#define LOG_COLOR_DEFAULT ""
#define LOG_COLOR_BLACK   ";30"
#define LOG_COLOR_RED     ";31"
#define LOG_COLOR_GREEN   ";32"
#define LOG_COLOR_BROWN   ";33"
#define LOG_COLOR_BLUE    ";34"
#define LOG_COLOR_PURPLE  ";35"
#define LOG_COLOR_CYAN    ";36"
#define LOG_COLOR_WHITE   ";37"
#define LOG_COLOR(color)  "\033[0" color "m"
#define LOG_BOLD(color)   "\033[1" color "m"
#define LOG_DIM(color)    "\033[2" color "m"
#define LOG_RESET_COLOR   "\033[0m"

// Characters to identify log message type
#define LOG_LV_ERROR_CHR   "E"
#define LOG_LV_WARN_CHR    "W"
#define LOG_LV_INFO_CHR    "I"
#define LOG_LV_DEBUG_CHR   "D"
#define LOG_LV_VERBOSE_CHR "V"

// Color mapping according to message type
#define LOG_LV_ERROR_COLOR   LOG_COLOR(LOG_COLOR_RED)
#define LOG_LV_WARN_COLOR    LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_LV_INFO_COLOR    LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_LV_DEBUG_COLOR
#define LOG_LV_VERBOSE_COLOR LOG_DIM(LOG_COLOR_DEFAULT)

// Avoid calling this directly, better use LOGE() ~ LOGV() below
#define LOG(level, fmt, ...) log_print(level ## _COLOR "[" \
		level ## _CHR "] %s: " fmt LOG_RESET_COLOR "\r\n", \
		__func__  __VA_OPT__(,) __VA_ARGS__)

#define LOG_FILTER(level, ...) do { if (level > LOG_LV_NONE && \
		level <= LOG_LEVEL_OUTPUT) \
	LOG(level, __VA_ARGS__); } while (0)

#define LOGE(...) LOG_FILTER(LOG_LV_ERROR, __VA_ARGS__)
#define LOGW(...) LOG_FILTER(LOG_LV_WARN, __VA_ARGS__)
#define LOGI(...) LOG_FILTER(LOG_LV_INFO, __VA_ARGS__)
#define LOGD(...) LOG_FILTER(LOG_LV_DEBUG, __VA_ARGS__)
#define LOGV(...) LOG_FILTER(LOG_LV_VERBOSE, __VA_ARGS__)

void log_init(uint64_t baudrate);
void log_print(const char *restrict fmt, ...);

#endif
