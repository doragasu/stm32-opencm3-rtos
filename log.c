#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/f4/nvic.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <stdarg.h>
#include <stdint.h>

#include "log.h"
#include "util.h"

#define LOG_BUF_LEN 1024


// Local log data
struct {
	uint_fast16_t pos;
	uint_fast16_t end;
	bool busy;
	char buf[LOG_BUF_LEN];
	SemaphoreHandle_t lock;
} ld = {};

static void dma_setup(void)
{
	// USART2 Tx uses DMA1, channel 4, stream 6
	rcc_periph_clock_enable(RCC_DMA1);
	nvic_enable_irq(NVIC_DMA1_STREAM6_IRQ);
	dma_stream_reset(DMA1, DMA_STREAM6);
	dma_set_priority(DMA1, DMA_STREAM6, DMA_SxCR_PL_LOW);
	dma_set_memory_size(DMA1, DMA_STREAM6, DMA_SxCR_MSIZE_8BIT);
	dma_set_peripheral_size(DMA1, DMA_STREAM6, DMA_SxCR_PSIZE_8BIT);
	dma_enable_memory_increment_mode(DMA1, DMA_STREAM6);
	dma_set_transfer_mode(DMA1, DMA_STREAM6,
				DMA_SxCR_DIR_MEM_TO_PERIPHERAL);
	dma_set_peripheral_address(DMA1, DMA_STREAM6, (uint32_t)&USART_DR(USART2));
	dma_enable_transfer_complete_interrupt(DMA1, DMA_STREAM6);
	dma_channel_select(DMA1, DMA_STREAM6, DMA_SxCR_CHSEL_4);
}

void log_init(uint64_t baudrate)
{
	ld.lock = xSemaphoreCreateBinary();
	xSemaphoreGive(ld.lock);
	// Enable clocks for GIPIOA and USART2
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_USART2);

	// Setup PA2 as Alternate Function for UART TX
	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO2);
	gpio_set_af(GPIOA, GPIO_AF7, GPIO2);

	usart_set_baudrate(USART2, baudrate);
	usart_set_databits(USART2, 8);
	usart_set_stopbits(USART2, USART_STOPBITS_1);
	usart_set_mode(USART2, USART_MODE_TX);
	usart_set_parity(USART2, USART_PARITY_NONE);
	usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);

	usart_enable_tx_dma(USART2);

	usart_enable(USART2);

	dma_setup();
}

static void dma_launch(const char *buf, uint_fast16_t len)
{
	dma_set_memory_address(DMA1, DMA_STREAM6, (uint32_t)buf);
	dma_set_number_of_data(DMA1, DMA_STREAM6, len);
	dma_enable_stream(DMA1, DMA_STREAM6);
}

void dma1_stream6_isr(void)
{
	if (dma_get_interrupt_flag(DMA1, DMA_STREAM6, DMA_TCIF)) {
		dma_clear_interrupt_flags(DMA1, DMA_STREAM6, DMA_TCIF);
		if (ld.pos != ld.end) {
			char *pos = ld.buf + ld.pos;
			uint_fast16_t len = ld.end - ld.pos;
			ld.pos = ld.end;
			dma_launch(pos, len);
		} else {
			ld.busy = false;
		}
	}
}

static uint_fast16_t buf_avail(void)
{
	return LOG_BUF_LEN - ld.end;
}

void log_print(const char *restrict fmt, ...)
{
	va_list va;
	int len;

	va_start(va, fmt);

	// Critical section, disable tasking and DMA interrupts
	xSemaphoreTake(ld.lock, portMAX_DELAY);
	nvic_disable_irq(NVIC_DMA1_STREAM6_IRQ);

	if (!ld.busy) {
		// No DMA transfer in progress
		len = vsnprintf(ld.buf, LOG_BUF_LEN, fmt, va);
		if (len <= 0) {
			goto out;
		}
		len = MIN(len, LOG_BUF_LEN - 1);
		ld.busy = true;
		ld.pos = ld.end = len;
		dma_launch(ld.buf, len);
	} else {
		// DMA transfer in progress, append data to the end
		char *pos = ld.buf + ld.end;
		int avail = buf_avail();
		len = vsnprintf(pos, avail, fmt, va);
		len = MIN(len, avail);
		if (len <= 0) {
			goto out;
		}
		ld.end += len;
	}

out:
	// Exit critical section
	nvic_enable_irq(NVIC_DMA1_STREAM6_IRQ);
	xSemaphoreGive(ld.lock);
}
