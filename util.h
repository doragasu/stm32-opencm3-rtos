#ifndef __UTIL_H__
#define __UTIL_H__

#define _STR(token) #token
#define STR(token) _STR(token)

#define MAX(a, b) (a) > (b) ? (a) : (b)
#define MIN(a, b) (a) < (b) ? (a) : (b)

#endif
